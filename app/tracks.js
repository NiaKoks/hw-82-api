const express = require('express');
const Track = require('../models/Track');

const router = express.Router();

router.get('/',(req,res)=>{
    if (req.query.album) {
        Track.find({album : req.query.album})
            .then(tracks => res.send(tracks))
            .catch(()=>res.sendStatus(500))
    }   Track.find()
            .then(tracks => res.send(tracks))
            .catch(()=> res.sendStatus(500));
});
router.post('/',(req,res)=>{
    const track = new Track(req.body);
    track.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

module.exports = router;